# standard imports
import os
import logging

# external imports
from eth_token_index import TokenUniqueSymbolIndex
from chaind.eth.token.erc20 import TokenResolver as ERC20TokenResolver
from chainlib.eth.address import is_address
from cic_eth_registry import CICRegistry
from cic_eth_registry.lookup.tokenindex import TokenIndexLookup

logg = logging.getLogger(__name__)


class TokenResolver(ERC20TokenResolver):

    def __init__(self, chain_spec, sender, signer, gas_oracle, nonce_oracle, settings=os.environ):
        super(TokenResolver, self).__init__(chain_spec, sender, signer, gas_oracle, nonce_oracle)
        self.lookup = TokenUniqueSymbolIndex(chain_spec, None, gas_oracle, nonce_oracle)
        self.registry_address = settings['CIC_REGISTRY_ADDRESS']


    def get_registry(self, conn):
        try:
            CICRegistry.init(self.registry_address, self.chain_spec, conn)
        except AttributeError as e:
            return CICRegistry(self.chain_spec, conn)

        registry = CICRegistry(self.chain_spec, conn) 
        token_registry_address = registry.by_name('TokenRegistry')
        logg.debug('using token registry address {}'.format(token_registry_address))
        lookup = TokenIndexLookup(self.chain_spec, token_registry_address)
        CICRegistry.add_lookup(lookup)
        return registry


    def create(self, conn, recipient, gas_value, data=None, token_value=0, executable_address=None, passphrase=None):

        if executable_address == None:
            raise ValueError('executable specifier required')

        if bool(executable_address) and not is_address(executable_address):
            registry = self.get_registry(conn)
            executable_address = registry.by_name(executable_address)

        return super(TokenResolver, self).create(conn, recipient, gas_value, data=data, token_value=token_value, executable_address=executable_address, passphrase=passphrase)
